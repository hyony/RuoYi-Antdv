import request from '@/utils/request'

// 获取路由
export const getRouters = () => {
  return new Promise((resolve,reject) => {
    setTimeout(() => {
      var json = require('@/api/mock/getRouters.json');
      resolve(json);
    }, 200)
  });
  // return request({
  //   url: '/getRouters',
  //   method: 'get'
  // })
}
